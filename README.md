# Aplicação para novos desenvolvedores javascript - Ignição digital

Olá, se você chegou até aqui, provavelmente se interessou por uma de nossas vagas em aberto para desenvolvedores javascript.


# Como fazer sua aplicação
Para se candidatar é simples:

+ Faça um fork deste repositório;

+ Crie um branch com o seu nome;

+ Para o teste prático, siga as instruções em  src/<teste>/index.html ou no .md dentro da pasta;

+ Envie um Pull Request utlizando o branch que você criou através do bitbucket;

# O que será avaliado

 1º Se o código é limpo, organizado e comentado;
 
 2º Se o código segue os conceitos do S.O.L.I.D;
 
 3º Uso de Design Patterns;
 
 4º Se foram confeccionados testes para o que foi codado;
 
 5º Boas práticas na manipulação de DOM;
 
 6º O funcionamento da aplicação;


Sinta-se à vontade para usar qualquer biblioteca externa como o AngularJS, JQuery, Sortable e demais, mas lembre-se que estamos avaliando seu código e a estrutura criada por VOCÊ.

Boa sorte!